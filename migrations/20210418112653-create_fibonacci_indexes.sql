
-- +migrate Up
-- +migrate StatementBegin
CREATE INDEX fibonacci_f_n_index
ON fibonacci (f_n);
-- +migrate StatementEnd


-- +migrate Down
DROP INDEX fibonacci_f_n_index;
