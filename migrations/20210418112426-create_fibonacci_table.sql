
-- +migrate Up
-- +migrate StatementBegin
CREATE TABLE fibonacci (
       n        INTEGER PRIMARY KEY CHECK (n >= 0),
       f_n      INTEGER NOT NULL CHECK (f_n >= 0)
);
-- +migrate StatementEnd

-- +migrate StatementBegin
GRANT SELECT, INSERT, UPDATE, TRUNCATE
ON fibonacci
TO fibonacci_http;
-- +migrate StatementEnd


-- +migrate Down
DROP TABLE fibonacci;
