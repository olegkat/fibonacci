package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path"

	"gitlab.com/olegkat/log"
)

const (
	defaultListenAddr         = ":8080"
	defaultWaitDB             = 30
	defaultDBConnStrMigration = "dbname=fibonacci user=fibonacci_migration"
	defaultDBConnStrHTTP      = "dbname=fibonacci user=fibonacci_http"
)

// Verbosity flag can be specified more than once to increment log level.
type flagVerbosityValue struct {
	logLevel *uint
}

func (f flagVerbosityValue) String() string {
	if f.logLevel == nil {
		return ""
	}

	return fmt.Sprintf("%d", *f.logLevel)
}

func (f flagVerbosityValue) Set(str string) error {
	*(f.logLevel)++
	if *f.logLevel >= log.LogMax {
		return errors.New(fmt.Sprintf("verbosity level requested past max (%d)", log.LogMax-1))
	}

	return nil
}

func (f flagVerbosityValue) IsBoolFlag() bool {
	return true
}

func init() {
	flag.Var(&flagVerbosityValue{&config.logLevel}, "v",
		fmt.Sprintf("increment verbosity level; can be given up to %d times", log.LogMax-1))

	flag.StringVar(&config.listenAddr, "l", defaultListenAddr,
		"address to listen on")

	flag.UintVar(&config.waitDB, "w", defaultWaitDB,
		"how many seconds to wait for the database to start accepting connections")

	flag.StringVar(&config.migrateDB, "m", "",
		"run DB migrations in the specified directory")

	flag.StringVar(&config.DBConnStrMigration, "x", defaultDBConnStrMigration,
		"DB connection string to use when running migrations")

	flag.StringVar(&config.DBConnStrHTTP, "y", defaultDBConnStrHTTP,
		"DB connection string to use when running the HTTP server")
}

func processFlags(config *Config, args []string) {
	config.progName = path.Base(os.Args[0])
	flag.Parse()
}
