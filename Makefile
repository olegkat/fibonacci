DB_HTTP_USER=fibonacci_http
DB_HTTP_PASSWORD=secret
DB_MIGRATION_USER=fibonacci_migration
DB_MIGRATION_PASSWORD=topsecret
GENERATED_TARGETS:=docker/app/pgpass docker/db/docker-entrypoint-initdb.d/10-create_db.sql dbconfig.yml

all: fibonacci generate docker

generate: $(GENERATED_TARGETS)

%: %.in
	sed \
	  -e "s/<DB_HTTP_USER>/$(DB_HTTP_USER)/g" \
	  -e "s/<DB_HTTP_PASSWORD>/$(DB_HTTP_PASSWORD)/g" \
	  -e "s/<DB_MIGRATION_USER>/$(DB_MIGRATION_USER)/g" \
	  -e "s/<DB_MIGRATION_PASSWORD>/$(DB_MIGRATION_PASSWORD)/g" \
	< $< > $@

docker: generate
	docker build -f docker/app/Dockerfile -t fibonacci-app .
	docker build -f docker/db/Dockerfile -t fibonacci-db .

fibonacci: *.go
	go build

test:
	go test

bench:
	go test -bench=.

clean:
	rm -f fibonacci
	rm -f $(GENERATED_TARGETS)
