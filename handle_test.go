package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime/debug"
	"testing"

	"github.com/ory/dockertest/v3"
)

func TestMain(m *testing.M) {
	// Start a docker container for the database.
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("could not connect to docker: %v", err)
	}

	resource, err := pool.Run("fibonacci-db", "latest", []string{})
	if err != nil {
		log.Fatalf("could not start DB container: %v", err)
	}

	connString := fmt.Sprintf("host=localhost dbname=fibonacci user=fibonacci_http port=%s password=secret", resource.GetPort("5432/tcp"))

	// Attempt to connect to the DB with exponential backoff-retry.
	if err := pool.Retry(func() error {
		var err error
		log.Printf("connecting to DB: %s", connString)
		db, err = sql.Open("pgx", connString)
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("could not connect to the database: %s", err)
	}
	log.Print("connected to DB.")

	// Apply migrations.
	config.DBConnStrMigration = fmt.Sprintf("host=localhost dbname=fibonacci user=fibonacci_migration port=%s password=topsecret", resource.GetPort("5432/tcp"))
	config.migrateDB = "./migrations"
	log.Printf("applying migrations to DB: %s", config.DBConnStrMigration)
	n, err := applyMigrations(config.DBConnStrMigration, "./migrations")
	if err != nil {
		log.Fatalf("error applying migrations: %v", err)
	}
	log.Printf("applied %d migrations.", n)

	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("could not purge resource: %v", err)
	}

	os.Exit(code)
}

func checkFibonacci(t *testing.T, expect []int64) {
	rows, err := db.Query("SELECT * FROM fibonacci ORDER BY n")
	if err != nil {
		t.Fatalf("%s\n%s", err, string(debug.Stack()))
	}
	defer rows.Close()

	var i int64
	for ; rows.Next(); i++ {
		if i >= int64(len(expect)) {
			t.Fatalf("Got more than expected %d rows\n%s", len(expect), string(debug.Stack()))
		}

		var n, f_n int64
		err = rows.Scan(&n, &f_n)
		if err != nil {
			t.Fatalf("%s\n%s", err, string(debug.Stack()))
		}

		t.Logf("Got (%d,%d)", n, f_n)
		if n != i || f_n != expect[i] {
			t.Fatalf("Got (%d,%d), expecting (%d,%d)\n%s", n, f_n, i, expect[i], string(debug.Stack()))
		}
	}
	if i != int64(len(expect)) {
		t.Fatalf("Got %d rows, expecting %d: %v\n%s", i, len(expect), expect, string(debug.Stack()))
	}
}

func TestCalcFi(t *testing.T) {
	clear()

	calcFi(3)
	checkFibonacci(t, []int64{0, 1, 1})

	calcFi(3)
	checkFibonacci(t, []int64{0, 1, 1, 2})

	calcFi(3)
	checkFibonacci(t, []int64{0, 1, 1, 2})

	calcFi(6)
	checkFibonacci(t, []int64{0, 1, 1, 2, 3})

	calcFi(6)
	checkFibonacci(t, []int64{0, 1, 1, 2, 3, 5})

	calcFi(6)
	checkFibonacci(t, []int64{0, 1, 1, 2, 3, 5, 8})

	calcFi(6)
	checkFibonacci(t, []int64{0, 1, 1, 2, 3, 5, 8})
}

func checkCount(t *testing.T, f_n, expect int64) {
	count, err := countFi(f_n)
	if err != nil {
		t.Fatalf("%s\n%s", err, string(debug.Stack()))
	}
	if count != expect {
		t.Fatalf("Got %d, expecting %d\n%s", count, expect, string(debug.Stack()))
	}
}

func TestCountFi(t *testing.T) {
	clear()

	calcFi(3)
	checkFibonacci(t, []int64{0, 1, 1})
	checkCount(t, 1, 1)
	checkCount(t, 2, 3)
	checkCount(t, 3, 3)

	calcFi(3)
	checkFibonacci(t, []int64{0, 1, 1, 2})
	checkCount(t, 1, 1)
	checkCount(t, 2, 3)
	checkCount(t, 3, 4)
	checkCount(t, 4, 4)
}

func BenchmarkCalcFi(b *testing.B) {
	rand.Seed(123)
	for i := 0; i < b.N; i++ {
		_, _, err := calcFi(rand.Int63n(20))
		if err != nil {
			b.Fatalf("error runninc calcFi: %v", err)
		}
	}
}
