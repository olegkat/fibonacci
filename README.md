Fibonacci sequence generator through a web API that memoizes
intermediate values.  The web API exposes operations to

1. fetch the Fibonacci number given an ordinal (e.g., Fib(11) == 89,
Fib(12) == 144),

1. fetch the number of memoized results less than a given value (e.g.,
there are 12 intermediate results less than 120), and

1. clear the data store.


[[_TOC_]]

# Running with docker

To build and run the web app and the database as a multi-container
docker app:

```
$ make
$ docker-compose up -d
```

Now access the [web app](http://localhost:8080/fib?n=5) from your
browser.  If everything went well, you will see `5`.

To stop the app:

```
$ docker-compose stop
```


# Running without docker

To build the binary and generate config files and scripts, run:

```
make generate
make fibonacci
```

Create (e.g., using `psql`) the fibonacci database and set up roles in
PostgreSQL, one for running migrations, and another for the HTTP
server:

```
CREATE DATABASE fibonacci;
CREATE USER fibonacci_migration PASSWORD 'topsecret';
GRANT ALL PRIVILEGES ON DATABASE fibonacci TO fibonacci_migration;
CREATE USER fibonacci_http PASSWORD 'secret';
```

Tell PostgreSQL to allow logins as these users from any local account,
e.g., by adding the following lines to your `pg_hba.conf`:

```
local   fibonacci       fibonacci_migrations password
local   fibonacci       fibonacci_http       password
```

(Don't forget to restart the server after modifying `pg_hba.conf`.)

Create `~/.pgpass` with the passwords you used for the PostgreSQL
roles above:

```
localhost:*:*:fibonacci_migration:topsecret
localhost:*:*:fibonacci_http:secret

```

Apply database migrations:

```
$ sql-migrate up
```

Run the web server:

```
./fibonacci -v -v -v
```

Now access the [web app](http://localhost:8080/fib?n=5) from your
browser.  If everything went well, you will see `5`.

# Endpoints

The web API exposes the following endpoints:

- `/fib?n=<uint>` fetch the Fibonacci number given the ordinal;
- `/count?f_n=<uint>` fetch the number of memoized results less than the given value;
- `/clear` clear the data store.

# Performance

The following is the output from running a benchmark on the `calcFi`
function on an Intel(R) Core(TM)2 Duo CPU P8700 @ 2.53GHz:

```
goos: linux
goarch: amd64
pkg: gitlab.com/olegkat/fibonacci
BenchmarkCalcFi-2   	     147	   8154980 ns/op
```
