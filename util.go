package main

import (
	"fmt"
	"net/http"
	"strings"
)

// ensureHTTPMethod checks that the request is with one of the given
// methods, and responds with 405 "Method Not Allowed" if it is not.
// Returns true if the request was one of the given methods.
func ensureHTTPMethod(w http.ResponseWriter, r *http.Request, methods []string) bool {
	for _, m := range methods {
		if r.Method == m {
			return true
		}
	}

	w.Header().Set("Allow", strings.Join(methods, ", "))
	w.WriteHeader(http.StatusMethodNotAllowed)
	w.Write([]byte(
		fmt.Sprintf("This endpoint can only be called using one of [%s].\n",
			strings.Join(methods, ", ")),
	))

	return false
}

// out500 logs errorArgs and responds with 500 "Internal Server
// Error".
func out500(w http.ResponseWriter, errorArgs ...interface{}) {
	logger.Error(errorArgs...)
	w.WriteHeader(http.StatusInternalServerError)
}
