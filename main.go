// Fibonacci sequence generator through a web API that memoizes
// intermediate values.  The web API exposes operations to (a) fetch
// the Fibonacci number given an ordinal (e.g., Fib(11) == 89, Fib(12)
// == 144), (b) fetch the number of memoized results less than a given
// value (e.g., there are 12 intermediate results less than 120), and
// (c) clear the data store.
package main

import (
	"context"
	"database/sql"
	"fmt"
	stdlog "log"
	"net/http"
	"os"
	"os/signal"
	"time"

	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/olegkat/log"
)

type Config struct {
	listenAddr         string
	logLevel           uint
	progName           string
	waitDB             uint
	migrateDB          string
	DBConnStrMigration string
	DBConnStrHTTP      string
}

var config Config
var logger *log.Logger

var db *sql.DB

// connectDB returns a database handle representing a pool of
// connections to the database using the provided connection string.
func connectDB(connString string) (db *sql.DB, err error) {
	connectStart := time.Now()
	tryUntil := connectStart.Add(time.Duration(config.waitDB) * time.Second)
	wait := time.Duration(1) * time.Second

	for {
		db, err = sql.Open("pgx", connString)
		if err == nil {
			err = db.Ping()
		}
		if err == nil || time.Now().After(tryUntil) {
			break
		}
		time.Sleep(wait)
		wait *= 2
	}
	if err != nil {
		err = fmt.Errorf("connectDB: timed out waiting to connect to database: %w", err)
		return
	}

	return
}

// applyMigrations connects to a database using the provided
// connection string and runs migrations in the provided directory.
// Returns the number of migrations applied.
func applyMigrations(connString, dir string) (n int, err error) {
	var db *sql.DB
	db, err = connectDB(config.DBConnStrMigration)
	if err != nil {
		err = fmt.Errorf("applyMigrations: %w", err)
		return
	}
	defer db.Close()

	migrations := &migrate.FileMigrationSource{
		Dir: config.migrateDB,
	}

	n, err = migrate.Exec(db, "postgres", migrations, migrate.Up)
	if err != nil {
		err = fmt.Errorf("applyMigrations: %w", err)
		return
	}

	return
}

func main() {
	processFlags(&config, os.Args)

	// Set up logging.
	var err error
	logger, err = log.New(
		os.Stderr,
		config.progName+": ",
		stdlog.LstdFlags|stdlog.Lmsgprefix,
		config.logLevel)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: error setting up logging: %v\n", config.progName, err)
		fmt.Fprintf(os.Stderr, "%s: exiting\n", config.progName)
		os.Exit(1)
	}

	// Connnect to the database.
	logger.Info("connecting to the database: " + config.DBConnStrHTTP)
	db, err = connectDB(config.DBConnStrHTTP)
	if err != nil {
		logger.Errorf("error connecting to the database: %v", err)
		logger.Error("exiting.")
		os.Exit(1)
	}
	logger.Info("connected to the database: " + config.DBConnStrHTTP)

	// Run DB migrations.
	if config.migrateDB != "" {
		logger.Infof("applying migrations in %s", config.migrateDB)

		n, err := applyMigrations(config.DBConnStrMigration, config.migrateDB)
		if err != nil {
			logger.Errorf("error applying migrations: %v", err)
			logger.Error("exiting.")
			os.Exit(1)
		}

		logger.Infof("applied %d migrations.", n)
	}

	// Configure endpoint handlers.
	mux := http.NewServeMux()
	mux.HandleFunc("/fib", handleFib)
	mux.HandleFunc("/count", handleCount)
	mux.HandleFunc("/clear", handleClear)
	// This catches / and all other (invalid) patterns.
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	})

	// Handle signals.
	srv := &http.Server{
		Handler: mux,
		Addr:    config.listenAddr,
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		logger.Info("interrupt signal handler set up.")
		<-sigint

		logger.Info("interrupt signal received, initiating graceful shutdown.")

		go func() {
			<-sigint
			logger.Info("second interrupt signal received, shutting down immediately.")
			os.Exit(1)
		}()

		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout.
			logger.Errorf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed) // notify the main goroutine
	}()

	// Start the HTTP server.
	logger.Infof("starting server on %s", config.listenAddr)

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener.
		logger.Fatalf("HTTP server ListenAndServe: %v", err)
	}

	// Graceful shutdown.
	logger.Info("waiting for idle HTTP connections to close...")
	<-idleConnsClosed
	logger.Info("idle HTTP connections closed.")

	logger.Info("waiting for DB queries to finish...")
	db.Close()

	logger.Info("exiting.")
}
