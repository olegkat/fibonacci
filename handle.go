package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"github.com/jackc/pgx/v4"
	pgxstd "github.com/jackc/pgx/v4/stdlib"
)

// calcFi calculates and caches F_i, where i <= n: (i, F_i) is not yet
// in the cache.  Returns i, F_i and an error, if any.
func calcFi(n int64) (i int64, f_i int64, err error) {
	var conn *pgx.Conn
	conn, err = pgxstd.AcquireConn(db)
	if err != nil {
		err = fmt.Errorf("calcFi: acquiring pgx.Conn: %w", err)
		return
	}

	defer func() {
		err = pgxstd.ReleaseConn(db, conn)
		if err != nil {
			err = fmt.Errorf("calcFi: releasing pgx.Conn: %w", err)
		}
	}()

	batch := &pgx.Batch{}
	batch.Queue(`
INSERT INTO fibonacci
VALUES (0, 0), (1, 1)
ON CONFLICT DO NOTHING
`)
	batch.Queue(`
INSERT INTO fibonacci
(
    SELECT max(n)+1, sum(f_n)
    FROM (
        SELECT n, f_n
        FROM fibonacci
        WHERE n < $1
        ORDER BY n DESC
        LIMIT 2
    ) foo
)
ON CONFLICT (n) DO UPDATE SET f_n = excluded.f_n
RETURNING n, f_n`, n) // ON CONFLICT ... UPDATE guarantees the query always returns a row

	res := conn.SendBatch(context.Background(), batch)
	if err != nil {
		err = fmt.Errorf("calcFi: sending batch: %w", err)
		return
	}
	defer func() {
		err = res.Close()
		if err != nil {
			err = fmt.Errorf("calcFi: closing batch result: %w", err)
		}

	}()

	_, err = res.Exec()
	if err != nil {
		err = fmt.Errorf("calcFi: exec: %w", err)
		return
	}

	err = res.QueryRow().Scan(&i, &f_i)
	if err != nil {
		err = fmt.Errorf("calcFi: calc: %w", err)
		return
	}

	return
}

// lookupFn returns the memoized Fibonacci number for the ordinal n.
func lookupFn(n int64) (f_n int64, err error) {
	err = db.QueryRow(`
SELECT f_n
FROM fibonacci
WHERE n = $1
`, n).Scan(&f_n)

	return
}

// handleFib handles the /fib endpoint.
func handleFib(w http.ResponseWriter, r *http.Request) {
	logger.Debugf("/fib: %v %v", r.Method, r.URL)

	if !ensureHTTPMethod(w, r, []string{http.MethodGet}) {
		return
	}

	out := func(f_n int64) {
		w.Write([]byte(fmt.Sprintln(f_n)))
	}

	// Parse URL params.
	q := r.URL.Query()
	ns, ok := q["n"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Parameter \"n\" is required.\n"))
		return
	}

	n, err := strconv.ParseInt(ns[0], 10, 64)
	if err != nil || n < 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error parsing parameter \"n\".\n"))
		return
	}

	// See if F_n is in the cache.
	logger.Debugf("handleFib: looking up F_%d", n)
	f_n, err := lookupFn(n)
	if err == nil {
		logger.Debug("handleFib: found: ", n, f_n)
		out(f_n)
		return
	}
	if err != sql.ErrNoRows {
		out500(w, "handleFib: querying fibonacci: ", err)
		return
	}

	// Not in the cache, need to calculate and cache.
	logger.Debugf("handleFib: calculating F_%d", n)
	if n == 0 { // special case #1
		_, err := db.Exec(`
INSERT INTO fibonacci
VALUES (0, 0)
ON CONFLICT DO NOTHING
`)
		if err != nil {
			out500(w, "handleFib: fibonacci <- (0,0): ", err)
			return
		}

		f_n = n

	} else if n == 1 { // special case #2
		_, err := db.Exec(`
INSERT INTO fibonacci
VALUES (0, 0), (1, 1)
ON CONFLICT DO NOTHING
`)
		if err != nil {
			out500(w, "handleFib: fibonacci <- (0,0), (1,1): ", err)
			return
		}

		f_n = n

	} else {
		for {
			var i int64
			i, f_n, err = calcFi(n)

			// pgx doesn't seem to always return an error
			// when there is one, so work around that by
			// also checking for i == 0.
			if err != nil || i == 0 {
				out500(w, "handleFib: error: i=", i, ", err=", err)
				return
			}
			logger.Debug("handleFib: calculated: ", i, f_n)

			if i == n {
				break
			}

			if i > n {
				out500(w, "handleFib: impossible insertion: ", i, n, f_n)
				return
			}
		}
	}

	out(f_n)
}

// countFi returns the count of the memoized Fibonacci numbers less
// than the given number.
func countFi(f_n int64) (count int64, err error) {
	err = db.QueryRow(`
SELECT count(*)
FROM fibonacci
WHERE f_n < $1
`, f_n).Scan(&count)

	return
}

// handleCount handles the /count endpoint.
func handleCount(w http.ResponseWriter, r *http.Request) {
	logger.Debugf("/count: %v %v", r.Method, r.URL)

	if !ensureHTTPMethod(w, r, []string{http.MethodGet}) {
		return
	}

	q := r.URL.Query()
	f_ns, ok := q["f_n"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Parameter \"f_n\" is required.\n"))
		return
	}

	f_n, err := strconv.ParseInt(f_ns[0], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error parsing parameter \"f_n\".\n"))
		return
	}

	count, err := countFi(f_n)
	if err != nil {
		out500(w, "handleCount: counting: ", err)
		return
	}

	w.Write([]byte(fmt.Sprintln(count)))
}

// clear truncates the data store table.
func clear() (err error) {
	_, err = db.Exec("TRUNCATE fibonacci")
	return
}

// handleClear handles the /clear endpoint.
func handleClear(w http.ResponseWriter, r *http.Request) {
	logger.Debugf("/clear: %v %v", r.Method, r.URL)

	if !ensureHTTPMethod(w, r, []string{http.MethodPost}) {
		return
	}

	err := clear()
	if err != nil {
		out500(w, "handleClear: ", err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
