module gitlab.com/olegkat/fibonacci

go 1.14

require (
	github.com/jackc/pgx/v4 v4.11.0
	github.com/ory/dockertest/v3 v3.6.3
	github.com/rubenv/sql-migrate v0.0.0-20210408115534-a32ed26c37ea
	gitlab.com/olegkat/log v0.0.0-20210227185103-3e4971baa8d6
)
